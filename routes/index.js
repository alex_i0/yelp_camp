const express = require('express');
const router = express.Router();
const passport = require('passport');
const User = require('../models/user');


//Root Route
router.get('/', function(req, res) {
    res.render('landing');
});


//==================AUTH ROUTES

//Show Register Route
router.get('/register', (req, res)=> {
    res.render('register');
});

//Handle SIgn Up Logic
router.post('/register', (req, res)=> {
    let newUser = new User({username: req.body.username});
    User.register(newUser, req.body.password, (err, user)=> {
        if(err){
            req.flash('error', err.message);
            return res.render('register');
        }
        passport.authenticate('local')(req, res, ()=> {
            req.flash('success', "Welcome to YelpCamp " + user.username);
            res.redirect('/campgrounds');
        });
    });
});

//LOGIN FORM

//show logic form
router.get('/login', (req, res)=> {
    res.render('login');
});

//handle login logic
router.post('/login', passport.authenticate('local', {
    successRedirect: '/campgrounds',
    failureRedirect: '/login'
}),(req, res)=> {
    
});

//LOGOUT ROUTE

//logout route
router.get('/logout', (req, res)=> {
    req.logout();
    req.flash('success', "Logged you out!");
    res.redirect('/campgrounds');
});


module.exports = router;