//ROUTES------------------------------------------------------------------//

const express = require('express');
const router = express.Router();
const Campground = require('../models/campground');
const middleware = require('../middleware');

//INDEX
router.get('/', (req, res)=> {
    
    Campground.find({}, (err, allCampgrounds)=>{
        if(err){
            console.log(err);
        } else{
            res.render('campgrounds/index', {campgrounds: allCampgrounds});
        }
    });
    
});


router.post("/", middleware.isLoggedIn, (req, res)=> {
    let name = req.body.name;
    let image = req.body.image;
    let description = req.body.description;
    let price = req.body.price;
    let author = {
        id: req.user._id,
        username: req.user.username
    };
    let newCampground = {name: name, image: image, description: description, price: price, author: author};
    
    Campground.create(newCampground, (err, newlyCreated)=>{
        if(err){
            console.log(err);
        }else{
            res.redirect('/campgrounds');     
        }
    });
});
//NEW
router.get('/new', middleware.isLoggedIn, (req, res)=> {
    res.render('campgrounds/new');
});
//SHOW 
router.get('/:id', (req, res)=>{
    
    Campground.findById(req.params.id).populate('comments').exec((err, foundCampground)=>{
        if(err || !foundCampground){
            req.flash('error', "Campground not found");
            res.redirect('back');
        }else{
            res.render('campgrounds/show', {campground: foundCampground});
        }
    })

    
});

//EDIT
router.get('/:id/edit', middleware.checkCampgroundOwnership, (req, res)=> {
     Campground.findById(req.params.id, (err, foundCampground)=> {
         res.render('campgrounds/edit', {campground: foundCampground});    
    });
});

//UPDATE
router.put('/:id', (req, res)=> {
    
    Campground.findByIdAndUpdate(req.params.id, req.body.campground, (err, updatedCampground)=> {
        if(err){
            res.redirect('/campgrounds');
        }else{
            res.redirect('/campgrounds/' + req.params.id);
        }
    });
});

//DELETE
router.delete('/:id', middleware.checkCampgroundOwnership,(req, res)=> {
    Campground.findByIdAndRemove(req.params.id, (err)=> {
        if(err){
            console.log(err);
            res.redirect('/campgrounds');
        }else{
            res.redirect('/campgrounds');
        }
    });
});


module.exports = router;