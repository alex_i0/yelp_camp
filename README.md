# yelp_camp

Yelp Camp is a complex web application based on MEN stack.

To run application:

1. git clone https://gitlab.com/alex_i0/yelp_camp.git
2. cd yelp_camp
3. npm install
4. npm start
5. Try and enjoy :)

*node.js is require to run application
*mongoDB is not require beacuse app is using database from mlab.com
